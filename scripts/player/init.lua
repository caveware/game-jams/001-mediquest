--[[
    Create the player object
  ]]--

player = {
    health  = 100,
    score   = 0
}

function player.setup()
    player.canJump     = false              -- Can the player jump?
    player.facing      = 1                  -- 1 for right, -1 for left
    player.image       = gfx.player.main    -- Current image for player
    player.imageStep   = 1                  -- Current animation step
    player.imageOver   = nil                -- Overriding image setting
    player.immuneTimer = 0                  -- Time left on immunity
    player.isImmune    = false              -- Is player immune?
    player.width       = 30                 -- Bound. width of player
    player.height      = 48                 -- Bound. of player
    player.x           = 240
    player.y           = 32
    player.xMaxSpeed   = 380                -- Maximum horizontal speed
    player.xSpeed      = 0
    player.ySpeed      = 0
end

function player.update(dt)
    -- Update the player every step

    -- Determine facing direction based on key presses
    local currentForce  = currentForce or 0
    if love.keyboard.isDown("left") then
        currentForce    = -1
    elseif love.keyboard.isDown("right") then
        currentForce    = 1
    end

    if currentForce < 0 then

        player.facing = -1

    elseif currentForce > 0 then

        player.facing = 1

    end

    -- Apply horizontal movement to player
    player.doHorizontal(dt, currentForce)
    -- Apply gravity to player
    player.doFalling(dt)

    -- Change player image when required
    if player.imageOver == "hit" then       -- After being hit
        player.image = gfx.player.hit[math.floor(player.imageStep)]
        player.imageStep    = player.imageStep + 3 * dt
        if player.imageStep >= (1 + #gfx.player.hit) then
            player.imageStep = 1
            player.imageOver = nil
        end
    elseif player.ySpeed ~= 0 then          -- Falling
        player.image        = gfx.player.falling
        player.imageStep    = 1
    elseif player.xSpeed == 0 then          -- Standing still
        player.image        = gfx.player.main
        player.imageStep    = 1
    elseif player.xSpeed ~= 0 then          -- Walking
        player.imageStep    = player.imageStep + 10 * dt * (math.abs(currentForce))
        if player.imageStep >= (1 + #gfx.player.walk) then
            player.imageStep = player.imageStep - #gfx.player.walk
        end
        player.image = gfx.player.walk[math.floor(player.imageStep)]
    end

    -- If player reaches level end, play sound and go to next level
    if levelEnd ~= nil then
        if checkCollision(player, levelEnd) then
            if levelEnd.nextLvl == "endGame" then
                world.gameEnding = true
            else
                love.audio.play(sndLevelEnd)
                map.clear()
                map.load("maps/" .. levelEnd.nextLvl)
            end
        end
    end

    -- If player is falling below map, start to kill
    if player.y + player.height > map.getHeight() then
        player.health = player.health - 500 * dt
    end

    -- If player touches a killzone, kill
    for k, v in ipairs(death) do
        if checkCollision(player, v) then
            player.health = player.health - 500 * dt
        end
    end

    -- If player touches a coin, delete it and increase score
    for k, v in ipairs(coins) do
        if checkCollision(player, v) then
            table.remove(coins, k)
            player.score = player.score + 1
            love.audio.stop(sndCoin)
            sndCoin:setPitch(0.9 + math.random(2) / 10) -- Spice it up
            love.audio.play(sndCoin)
        end
    end

    -- If player touches a diamond, delete it and increase score
    for k, v in ipairs(diamonds) do
        if checkCollision(player, v) then
            table.remove(diamonds, k)
            player.score = player.score + 10
            love.audio.stop(sndDiamond)
            love.audio.play(sndDiamond)
        end
    end

    -- If player touches a heart, delete it and increase health
    for k, v in ipairs(health) do
        if checkCollision(player, v) and player.health < 100 then
            table.remove(health, k)
            player.health = player.health + 50
            love.audio.stop(sndHealth)
            love.audio.play(sndHealth)
            if player.health > 100 then
                player.health = 100
            end
        end
    end

    -- If player touches an enemy:
    for k, v in ipairs(enemies) do
        if checkCollision(player, v) then
            player.x = math.round(player.x)
            player.y = math.round(player.y)
            -- If player lands on enemy's head:
            if player.ySpeed > v.ySpeed and player.y + player.height > v.y then
                while checkCollision(player, v) do
                    player.y = player.y - math.sign(player.ySpeed) -- Move player until no longer colliding with enemy
                end
                player.ySpeed = - player.ySpeed -- Reverse player's vertical speed, "bouncing" player
                v.hits = v.hits - 1 -- Reduce enemy's HP by 1
                if v.hits == 0 then
                    table.remove(enemies, k)
                    love.audio.stop(sndEnemyDie)
                    love.audio.play(sndEnemyDie)
                    player.score = player.score + v.value
                end
            else
                player.giveDamage(v.strength)
            end
        end
    end

    -- If player touches a spike, make immune and cause damage
    for k, v in ipairs(spikes) do
        if checkCollision(player, v) then
            player.giveDamage(20)
        end
    end

    -- Decrease immuneTimer if it is set
    if player.immuneTimer > 0 then
        player.immuneTimer = player.immuneTimer - 10 * dt
    else
        player.immuneTimer = 0
        player.isImmune = false
    end

    -- If health is smaller than zero, reset and restart the room
    if player.health <= 0 then
        love.audio.play(sndDeath)
        player.health   = 100
        player.score    = 0
        world.deathCounter  = world.deathCounter + 1
        map.restart()
    end
end

function player.draw(vx, vy)
    -- This creates the flashing effect desired on being immune
    if player.isImmune and math.ceil(player.immuneTimer)/2 == math.ceil(math.ceil(player.immuneTimer)/2) then
        -- DO SHIT ALL
    else
        if player.facing < 0 then       -- Flip character if facing left
            vx = vx - player.width          -- Add extra space if flipping character
        end

        -- This is purely to keep code readable
        local x, y
        x   = math.round(player.x - vx) - 1
        y   = math.round(player.y - vy) - (player.image:getHeight() - player.height)

        -- Draw player according to modifiers
        love.graphics.draw(player.image, x, y, 0, player.facing, 1)
    end
end

function player.giveDamage(damage)
    -- Damage the player a certain amount then inflict immunity
    if not player.isImmune then
        player.imageOver    = "hit"
        player.health       = player.health - damage
        player.isImmune     = true
        player.immuneTimer  = 20
        love.audio.stop(sndHurt)
        love.audio.play(sndHurt)
    end
end

function player.doHorizontal(dt, dir)
    -- Calculate horizontal movement
    local friction, xSpeedSign
    if player.imageOver == "hit" then
        dir = 0
    end

    -- Determine friction based on canJump, which may as well be called 'onGround'
    if player.canJump then
        friction = world.groundFriction
    else
        friction = world.airFriction
    end

    -- If no button pressed then slow player down
    if dir == 0 then
        local prevSign
        -- Determine current sign of xSpeed
        prevSign = math.sign(player.xSpeed)
        player.xSpeed = player.xSpeed - (7000 * prevSign * dt * friction)
        if math.sign(player.xSpeed) ~= prevSign then
            player.xSpeed = 0
        end
    -- If a button actually has been pressed, speed up player
    else
        player.xSpeed = player.xSpeed + (8000 * dir * dt * friction)
        if math.abs(player.xSpeed) > player.xMaxSpeed then
            player.xSpeed = player.xMaxSpeed * dir
        end
    end

    -- Move player - simple as that
    player.x = player.x + player.xSpeed * dt

    -- Determine if player collides with solid map segments
    if map.touches(player) then
        -- Round the player's y for easier collision detection
        player.x  = math.round(player.x)

        -- Move player in direction away from block until free of collisions
        while map.touches(player) do
            player.x    = player.x - math.sign(player.xSpeed)
        end

        -- Halt player's horizontal movement
        player.xSpeed   = 0
    end

    -- If player is horizontally off the edge of the map, correct it
    if player.x < 0 then
        player.x        = 0
        player.xSpeed   = 0
    elseif (player.x + player.width) > map.getWidth() then
        player.x        = map.getWidth() - player.width
        player.xSpeed   = 0
    end
end

function player.doFalling(dt)
    -- Calculate everything related to the player's vertical speed

    -- Determine if player is on a ladder
    touchLadder = false
    for k, v in ipairs(ladders) do
        if checkCollision(player, v) then
            touchLadder = true
        end
    end

    -- If on a ladder, determine ySpeed based on keys pressed
    if touchLadder then
        local getVert = getVert or 0
        if love.keyboard.isDown("down") then
            getVert = 400
        elseif love.keyboard.isDown("up") then
            getVert = -400
        end

        player.ySpeed = getVert * 400
    -- If not on a ladder, apply regular gravity
    else
        player.ySpeed = player.ySpeed + (world.gravity * dt)
    end

    -- If player's falling speed is larger than terminal velocity, limit it
    if player.ySpeed > world.terminalVelocity then
        player.ySpeed = world.terminalVelocity
    end

    -- Move the player vertically
    player.y = player.y + player.ySpeed * dt

    -- Determine if player collides with solid map segments
    if map.touches(player) then
        -- Round the player's y for easier collision detection
        player.y  = math.round(player.y)

        -- Move player in direction away from block until free of collisions
        while map.touches(player) do
            player.y    = player.y - math.sign(player.ySpeed)
        end

        -- If player has hit a block below itself, allow it to jump
        if player.ySpeed > 0 then
            player.canJump = true
        -- Otherwise do not and move player down to avoid being stuck
        else
            player.y = player.y + 1
        end

        -- Stop player's vertical movement
        player.ySpeed   = 0
    end

    -- If player is falling, do not allow it to jump
    if player.ySpeed ~= 0 then
        player.canJump  = false
    end

    -- If conditions are met, allow player to jump
    if player.canJump and (love.keyboard.isDown(" ") or toJump) and player.imageOver ~= "hit" and not world.gameEnding then
        player.ySpeed   = -1250
        player.canJump  = false
        sndJump:setPitch(0.7 + math.random(6) / 10) -- Spice it up
        love.audio.play(sndJump)
    end
end

function player.totemMove()
    if totem.isActivated then
        map.load(totem.level, "nope")
        player.x = totem.x + (totem.width / 2) - (player.width / 2)
        player.y = totem.y - (player.height - totem.height)-2
        player.ySpeed = -1
        if map.touches(player) then
            player.x    = math.round(player.x)
            player.y    = math.round(player.y)
            while map.touches(player) do
                player.y = player.y + math.sign(player.ySpeed)
            end
        end
        love.audio.play(sndTotemSave)
        if world.haveMedicine then
            player.facing = -1
        else
            player.facing = 1
        end
    end
end
