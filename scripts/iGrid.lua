-- chikun :: 2015
-- Image grids for particle effects


-- Will contain image grid functions
iGrid = { }


-- Loads all possible particle effects
pfx = {
    spin    = require 'pfx/spin',
    vertAsh = require 'pfx/vertAsh'
}



-- Creates a new image grid
function iGrid.new(image, size)


    -- Get image size
    local iw, ih = image:getWidth(), image:getHeight()


    -- Create new grid
    newGrid = {
        w   = iw,
        h   = ih,
        ox  = iw / 2,
        oy  = ih / 2
    }


    -- Actually create the grid
    for y = 0, ih, size do

        for x = 0, iw, size do

            newGrid[#newGrid + 1] = {
                quad    = love.graphics.newQuad(x, y, size, size, iw, ih),
                x       = x,
                y       = y,
                w       = size,
                h       = size,
                id      = #newGrid + 1,
                alpha   = 1,
                colour  = { 255, 255, 255 }
            }

        end
    end


    -- Returns the new image grid
    return newGrid


end



-- Updates the image grid and its effects
function iGrid.update(grid, dt)


    -- If the grid has an effect...
    if grid.effect then

        -- ...update it
        grid.effect:update(grid, dt)

    end


end



-- Draws the image grid
function iGrid.draw(grid, image, x, y)


    local cCol = {love.graphics.getColor()}

    local colScal = {
        cCol[1] / 255,
        cCol[2] / 255,
        cCol[3] / 255,
        cCol[4] / 255
    }

    -- For all quads in this grid...
    for key, value in ipairs(grid) do

        -- ...set colour/alpha of quad
        love.graphics.setColor(value.colour[1] * colScal[1],
            value.colour[2] * colScal[2],
            value.colour[3] * colScal[3],
            value.alpha * 255 * colScal[4])

        -- ...draw quad
        love.graphics.draw(image, value.quad, x + value.x, y + value.y)

    end


end



-- Applies a particle effect to the grid
function iGrid.applyEffect(grid, effect, speed)


    -- Speed of effect, defaults to 64
    speed = speed or 64

    -- Sets the new effect to the grid
    grid.effect = effect

    -- First-time setup of effect
    grid.effect:apply(grid, speed)


end
