--[[
    Loads sounds required in-game
  ]]--

sndDeath    = love.audio.newSource("sfx/player/death.ogg", "static")        -- Player dies
sndHurt     = love.audio.newSource("sfx/player/hurt.ogg", "static")         -- Player incurs damage
sndJump     = love.audio.newSource("sfx/player/jump.ogg", "static")         -- Player jumps

sndCoin     = love.audio.newSource("sfx/powerup/coin.ogg", "static")        -- Player collects coin
sndDiamond  = love.audio.newSource("sfx/powerup/diamond.ogg", "static")     -- Player collects diamond
sndHealth   = love.audio.newSource("sfx/powerup/health.ogg", "static")      -- Player collects health

sndBlockFall= love.audio.newSource("sfx/world/blockFall.ogg", "static")     -- Falling block is activated
sndLevelEnd = love.audio.newSource("sfx/world/levelEnd.ogg", "static")      -- Player reaches end of level

sndTotem1   = love.audio.newSource("sfx/totem/place_1.ogg", "static")
sndTotem2   = love.audio.newSource("sfx/totem/place_2.ogg", "static")
sndTotem3   = love.audio.newSource("sfx/totem/place_3.ogg", "static")
sndTotemFail= love.audio.newSource("sfx/totem/denyUse.ogg", "static")
sndTotemSave= love.audio.newSource("sfx/totem/save.ogg", "static")

sndEnemyDie = love.audio.newSource("sfx/enemy/death.ogg", "static")         -- Enemy is destroyed

bgmCohen1   = love.audio.newSource("bgm/cohen_1.ogg")
bgmCohen2   = love.audio.newSource("bgm/cohen_2.ogg")
bgmCohen3   = love.audio.newSource("bgm/cohen_3.ogg")
bgmMedicine = love.audio.newSource("bgm/medicine.ogg")
bgmCohen1:setLooping(true)      ;   bgmCohen2:setLooping(true)
bgmCohen3:setLooping(true)      ;   bgmMedicine:setLooping(true)
bgmCohen1:setVolume(1)          ;   bgmCohen2:setVolume(0.6)
bgmCohen3:setVolume(1)          ;   bgmMedicine:setVolume(0.75)

function startMusic(level)
    if (level == "level01" or level == "level02" or
        level == "level06") and not bgmCohen1:isPlaying()
    then
        love.audio.stop()
        love.audio.play(bgmCohen1)
    elseif (level == "level03" or level == "level04" or
        level == "level05" or level == "level10") and not bgmCohen2:isPlaying()
    then
        love.audio.stop()
        love.audio.play(bgmCohen2)
    elseif (level == "level07" or level == "level08" or level == "level09") and not bgmCohen3:isPlaying()
    then
        love.audio.stop()
        love.audio.play(bgmCohen3)
    elseif level == "levelMedicine" and not bgmMedicine:isPlaying()
    then
        love.audio.stop()
        love.audio.play(bgmMedicine)
    elseif level == "level00" then
        love.audio.stop()
    end
end
