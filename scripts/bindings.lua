-- chikun :: 2014
-- Shorthand bindings for more optimised code

--[[
    Omitted these libraries due to our lack of use
    * love.font
    * love.image
    * love.math
    * love.sound
    * love.thread
]]--


--[[
    We use these bindings so that our code can be shorter and more consise.
    As such, we generally have to avoid using single-letter variable names.
    That's okay though, as our code becomes more readable because of this.
  ]]


a = love.audio
e = love.event
f = love.filesystem
g = love.graphics
j = love.joystick
k = love.keyboard
m = love.mouse
s = love.system
t = love.timer
w = love.window


--[[
    As an aside, you should go to love2d.org/wiki and read up on all of
    these libraries. This essential to becoming good at coding this engine.
  ]]
