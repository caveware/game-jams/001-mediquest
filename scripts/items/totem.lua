--[[
    Creates the totem pole needed to check a point
  ]]--

totem = {
    image       = gfx.totem.main,
    isActivated = false,
    level       = "",
    timer       = 0,
    x           = 0,
    y           = 534,
    width       = 48,
    height      = 44
}

function totem.update(dt)
    if totem.timer > 0 then
        local tmp1, tmp2
        tmp1        = math.ceil(totem.timer)
        totem.timer = totem.timer - 3 * dt
        tmp2        = math.ceil(totem.timer)

        -- Sounds
        if tmp1 == 3 and tmp2 == 2 then
            love.audio.play(sndTotem1)
        elseif tmp1 == 2 and tmp2 == 1 then
            love.audio.play(sndTotem2)
        elseif tmp1 == 1 and tmp2 == 0 then
            love.audio.play(sndTotem3)
        end

        if tmp2 > 0 then
            totem.image = gfx.totem.glow[4 - tmp2]
        else
            totem.timer = 0
            totem.image = gfx.totem.main
        end
    end
end

function totem.place()
    if (love.keyboard.isDown("lctrl") or totemPlace) and player.canJump and not totem.isActivated and not (levelName == "maps/level10" and player.x > 928) then
        totem.x             = player.x + (player.width / 2) - (totem.width / 2)
        totem.y             = player.y + (player.height - totem.height)
        totem.isActivated   = true
        totem.level         = levelName
        totem.timer = 3
    elseif levelName == "maps/level10" or levelName == "maps/levelMedicine" then
        love.audio.play(sndTotemFail)
    end
end

function totem.draw(vx, vy)
    if checkCollision(totem, view) and totem.level == levelName and totem.isActivated then
        love.graphics.draw(totem.image, totem.x - vx, totem.y - vy)
    end
end
