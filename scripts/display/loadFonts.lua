--[[
    Load fonts needed for the game
  ]]--

fontScore       = love.graphics.newFont("fnt/alagard.ttf", 33)        -- Used in GUI: SCORE
fontHeader      = love.graphics.newFont("fnt/alagard.ttf", 22)        -- Used in GUI: SCORE
fontText        = love.graphics.newFont("fnt/alagard.ttf", 16)        -- Used in GUI: SCORE
