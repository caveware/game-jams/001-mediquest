--[[
    Extra mathematical functions
  ]]--

math.randomseed(os.time())          -- Set random seed so that random(x) functions work properly

function math.round(value)
    -- Rounds a value - should be default
    return math.floor(value + 0.5)
end

function math.sign(value)
    -- Returns the sign of a supplied variable
    local tmp
    tmp = 0
    if value > 0 then
        tmp = 1
    elseif value < 0 then
        tmp = -1
    end
    return tmp
end

function tableCopy(orig)
    -- Copies a supplied table into a completely new table
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in pairs(orig) do
            copy[orig_key] = orig_value
        end
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end
