-- chikun :: 2014
-- Loads all graphics from the /gfx folder


-- Recursively checks a folder for graphics
-- and adds any found to a gfx table

love.graphics.setNewFont('fnt/alagard.ttf', 22)

function checkFolder(dir, tab)

    local tab = ( tab or { } )

    -- Get a table of files / subdirs from dir
    local items = f.getDirectoryItems(dir)

    for key, val in ipairs(items) do

        if f.isFile(dir .. "/" .. val) then
            -- If item is a file, then load it into tab

            -- As long as it's not a Thumbs.db
            if val ~= "Thumbs.db" then

                -- Remove ".png" extension on file
                local name = val:sub(1, val:len() - 4)

                -- Load image into table
                tab[name] = g.newImage(dir .. "/" .. val)

                resLoaded = resLoaded + 1

            end

        else
            -- Else, run checkFolder on subdir

            -- Add new table onto tab
            tab[val] = { }

            -- Run checkFolder in this subdir
            checkFolder(dir .. "/" .. val, tab[val])

        end

        drawRes(resLoaded)

    end

    return tab

end


function drawRes(a)

    love.graphics.setCanvas(gameCanvas)

    love.graphics.setColor(0, 0, 0)

    love.graphics.rectangle('fill', 0, 0, 854, 480)

    love.graphics.setColor(255, 255, 255)

    local scal = resLoaded / resNum

    love.graphics.rectangle('fill', 127, 220, 600 * scal, 40)

    love.graphics.rectangle('line', 127, 220, 600, 40)

    love.graphics.print('Loading...', 8, 8)

    love.graphics.setCanvas()

    rescale()

    love.graphics.present()

end

resLoaded = 0

resNum = 0

local function checkDir(dir)

    -- Get a table of files / subdirs from dir
    local items = f.getDirectoryItems(dir)

    for key, val in ipairs(items) do

        if f.isFile(dir .. "/" .. val) then

            if val ~= "Thumbs.db" then
                resNum = resNum + 1

            end

        else

            checkDir(dir .. "/" .. val)

        end
    end
end

checkDir('gfx')

drawRes(0)

-- Load the gfx folder into the gfx table
gfx = checkFolder("gfx")

-- Kills checkFolder
checkFolder = nil
