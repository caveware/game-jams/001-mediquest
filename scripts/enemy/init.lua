--[[
    Create the enemy object
  ]]--

enemy   = { }

enemyTypes  = {
    grunt   = {
        strength    = 20,
        value       = 5,
        image       = gfx.enemy.grunt,
        x           = 300,
        y           = 0,
        xSpeed      = 350,
        ySpeed      = 0,
        width       = 64,
        height      = 64,
        hits        = 1,
        flying      = false,
    },
    skelly  = {
        strength    = 20,
        value       = 5,
        image       = gfx.enemy.skelly,
        x           = 300,
        y           = 0,
        xSpeed      = 350,
        ySpeed      = 0,
        width       = 64,
        height      = 64,
        hits        = 1,
        flying      = false,
    },
    wasp    = {
        strength    = 20,
        value       = 10,
        image       = gfx.enemy.wasp,
        x           = 300,
        y           = 0,
        xSpeed      = 350,
        ySpeed      = 0,
        width       = 64,
        height      = 64,
        hits        = 1,
        flying      = true,
    },
    goblin  = {
        strength    = 35,
        value       = 20,
        image       = gfx.enemy.goblin,
        x           = 300,
        y           = 0,
        xSpeed      = 350,
        ySpeed      = 0,
        width       = 64,
        height      = 64,
        hits        = 2,
        flying      = false,
    },
    floaty  = {
        strength    = 35,
        value       = 30,
        image       = gfx.enemy.floaty,
        x           = 300,
        y           = 0,
        xSpeed      = 350,
        ySpeed      = 0,
        width       = 64,
        height      = 64,
        hits        = 2,
        flying      = true,
    },
    dkskel  = {
        strength    = 40,
        value       = 50,
        image       = gfx.enemy.dkskel,
        x           = 300,
        y           = 0,
        xSpeed      = 350,
        ySpeed      = 0,
        width       = 64,
        height      = 64,
        hits        = 2,
        flying      = false,
    },
    boss    = {
        strength    = 50,
        value       = 100,
        image       = gfx.enemy.boss,
        x           = 300,
        y           = 0,
        xSpeed      = 0,
        ySpeed      = 0,
        width       = 64,
        height      = 64,
        hits        = 3,
        flying      = false,
    },
}

function newEnemy(varType, varX, varY, varWidth, varHeight)
    enemies[#enemies + 1] = {
        type        = varType,
        x           = varX,
        y           = varY,
        width       = varWidth,
        height      = varHeight,
        xSpeed      = varType.xSpeed,
        ySpeed      = varType.ySpeed,
        strength    = varType.strength,
        value       = varType.value,
        hits        = varType.hits,
        flying      = varType.flying,
    }
end


function enemy.update(dt)
    for k, v in ipairs(enemies) do
        v.x = v.x + v.xSpeed * dt

        if v.type == enemyTypes.boss then
            if math.abs(v.xSpeed) > 500 then
                v.xSpeed = math.sign(v.xSpeed) * 500
            end
            if player.x > map.getWidth() - 1024 then
                if player.x < v.x then
                    v.xSpeed = v.xSpeed - 30
                end
                if v.x < player.x then
                    v.xSpeed = v.xSpeed + 30
                end
            end

        else
            if math.abs(v.xSpeed) > 350 then
                v.xSpeed = 350 * math.sign(v.xSpeed)
            end
        end

        if map.touches(v) then
            v.x             = math.round(v.x)
            v.xSpeed        = -v.xSpeed
            while map.touches(v) do
                v.x = v.x + math.sign(v.xSpeed)
            end
        end
        if v.flying == false then                   -- This code doesn't affect flying creatures
            local enemyClone    = tableCopy(v)
            enemyClone.x        = v.x + math.sign(v.xSpeed) * v.width
            enemyClone.y        = v.y + 2           -- Changed to a plus 2 and it works
            if not map.touches(enemyClone) then
                v.x             = math.round(v.x)
                v.xSpeed        = -v.xSpeed
            end
        end
    end
end


function enemy.draw(vx, vy)
    for k, v in ipairs(enemies) do
        local swap = 1
        if v.xSpeed < 0 then
            swap = -1
        end

        -- This is purely to keep code readable
        local x, y
        x   = math.round(v.x - vx)
        if v.xSpeed < 0 then
            x = x + v.width
        end
        y   = math.round(v.y - vy)
        love.graphics.draw(v.type.image, x, y, 0, swap, 1)
    end
end
