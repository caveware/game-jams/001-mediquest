-- Configuration for the Ludum Dare game
function love.conf(t)
    t.window.width  = 854;
    t.window.height = 480;
    t.window.minwidth = 854
    t.window.minheight = 480
    t.window.vsync = true
    t.window.resizable = true;
    t.window.title  = "MediQuest";
    t.window.icon   = "gfx/window/icon.png"
end
